from django.shortcuts import render
from api.models import User, Task, List, Board, Team
from rest_framework import generics
from api.serializers import \
    BoardSerializer, UserSerializer, TaskSerializer, \
    ListSerializer, UserDetailSerializer, BoardDetailSerializer, ListDetailSerializer
from rest_framework import viewsets
from api.permissions import IsOwnerOrReadOnly
from rest_framework.permissions import IsAuthenticatedOrReadOnly, IsAuthenticated
from django.shortcuts import get_object_or_404
from rest_framework.response import Response
# Create your views here.


class BoardViewSet(viewsets.ModelViewSet):
    queryset = Board.objects.all()
    serializer_class = BoardSerializer
    permission_classes = (
        IsAuthenticatedOrReadOnly,
        IsOwnerOrReadOnly,)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def retrieve(self, request, pk=None):
        print("Inside retrieve")
        queryset = self.queryset
        board = get_object_or_404(queryset, pk=pk)
        serializer = BoardDetailSerializer(board)
        return Response(serializer.data)


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (
        IsAuthenticatedOrReadOnly,
        IsOwnerOrReadOnly,)


class ListViewSet(viewsets.ModelViewSet):
    queryset = List.objects.all()
    serializer_class = ListSerializer
    permission_classes = (
        IsAuthenticated,
    )

    def retrieve(self, request, pk=None):
        print("Inside retrieve")
        queryset = self.queryset
        task_list = get_object_or_404(queryset, pk=pk)
        serializer = ListDetailSerializer(task_list)
        return Response(serializer.data)


class TaskViewSet(viewsets.ModelViewSet):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer
    permission_classes = (
        IsAuthenticatedOrReadOnly,
        IsOwnerOrReadOnly,
    )

    def perform_create(self, serializer):
        print("This works:::::::::::::::::;")
        serializer.save(user=self.request.user)
