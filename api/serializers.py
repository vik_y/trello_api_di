from rest_framework import serializers
from api.models import User, Task, Board, List, Team


class TaskSerializer(serializers.ModelSerializer):
    user = serializers.PrimaryKeyRelatedField(read_only=True)
    class Meta:
        model = Task
        fields = "__all__"


class ListSerializer(serializers.ModelSerializer):
    class Meta:
        model = List
        fields = ('id', 'name', 'board')


class ListDetailSerializer(serializers.ModelSerializer):
    tasks = TaskSerializer(read_only=True, many=True)
    class Meta:
        model = List
        fields = ('id', 'name', 'board', 'tasks')


class BoardSerializer(serializers.ModelSerializer):
    #lists = ListSerializer(many=True, read_only=True)

    class Meta:
        model = Board
        fields = ('id', 'user', 'name',)


class BoardDetailSerializer(serializers.ModelSerializer):
    lists = ListSerializer(many=True, read_only=True)

    class Meta:
        model = Board
        fields = ('id', 'user', 'name', 'lists', 'user',)


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username',)


class UserDetailSerializer(serializers.ModelSerializer):
    boards = BoardSerializer(read_only=True)
    tasks = TaskSerializer(read_only=True)

    class Meta:
        model = User
        fields = ('id', 'username', 'boards',)



class TeamSerializer(serializers.ModelSerializer):
    class Meta:
        model = Team
        fields = '__all__'




