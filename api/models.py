from django.db import models
from django.contrib.auth.models import User


# Create your models here.
# TODO: Django model basic fields
# TODO: Relations
# TODO : Migrations
# TODO: Shell


# Additional things to do :
# TODO: Integrate postgres db , BONUS
# models.ForeignKey

class Team(models.Model):
    name = models.CharField(max_length=100)
    members = models.ManyToManyField(User)


class Board(models.Model):
    name = models.CharField(max_length=100)
    user = models.ForeignKey(User, on_delete=models.CASCADE, default=1, related_name='boards')


class List(models.Model):
    name = models.CharField(max_length=100)
    board = models.ForeignKey(Board, on_delete=models.CASCADE, related_name='lists')


class Task(models.Model):
    list = models.ForeignKey(List, on_delete=models.CASCADE, related_name='tasks')
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='tasks')
    data = models.TextField()




